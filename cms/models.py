from django.db import models
import datetime
from django.contrib.auth.models import User

# Create your models here.


class Book(models.Model):
    """書籍"""
    name = models.CharField('書籍名', max_length=255)
    publisher = models.CharField('出版社', max_length=255, blank=True)
    page = models.IntegerField('ページ数', blank=True, default=0)

    def __str__(self):
        return self.name


class Impression(models.Model):
    """感想"""
    book = models.ForeignKey(Book, verbose_name='書籍', related_name='impressions')
    comment = models.TextField('コメント', blank=True)

    def __str__(self):
        return self.comment


class WhiteBorde(models.Model):
    """ホワイトボード"""
    now = datetime.datetime.now()

    # shain_id = models.CharField('社員ID', max_length=255, primary_key=True)
    staff = models.OneToOneField('Staff', verbose_name='社員番号', related_name='whitebord')
    status = models.CharField('出退勤状況', max_length=10)
    work = models.ForeignKey('Place', verbose_name='作業場所', related_name='whitebord', blank=True, null=True)
    biko = models.CharField('備考', max_length=255, blank=True)
    updatedate = models.DateTimeField('最終更新日', auto_now=True, blank=True, null=True)

    def __str__(self):
        return self.staff.name


class Staff(models.Model):
    """社員"""

    user = models.OneToOneField(User, verbose_name='ユーザ', related_name='staff')
    name = models.CharField('ユーザ名', max_length=255)
    section = models.ForeignKey('Section', verbose_name='セクション', related_name='staff', blank=True, null=True)
    position = models.ForeignKey('Position', verbose_name='ポジション', related_name='staff', blank=True, null=True)

    def __str__(self):
        return self.user.username


class Section(models.Model):
    """セクションマスタ"""

    section = models.CharField('セクション', max_length=255)
    is_edit = models.BooleanField('修正可能フラグ')

    def __str__(self):
        return self.section


class Position(models.Model):
    """ポジションマスタ"""

    position = models.CharField('ポジション', max_length=255)
    is_edit = models.BooleanField('修正可能フラグ')

    def __str__(self):
        return self.position


class Place(models.Model):
    """作業場所マスタ"""

    place = models.CharField('作業場所', max_length=255)

    def __str__(self):
        return self.place