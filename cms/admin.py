from django.contrib import admin

# Register your models here.
from cms.models import Book, Impression, WhiteBorde, Staff, Section, Position, Place

# admin.site.register(Book)
# admin.site.register(Impression)


class BookAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'publisher', 'page',)  # 一覧に出したい項目
    list_display_links = ('id', 'name',)  # 修正リンクでクリックできる項目
admin.site.register(Book, BookAdmin)


class ImpressionAdmin(admin.ModelAdmin):
    list_display = ('id', 'comment',)
    list_display_links = ('id', 'comment',)
admin.site.register(Impression, ImpressionAdmin)


class WhiteBordeAdmin(admin.ModelAdmin):
    list_display = ('id', 'staff', 'status', 'work', 'biko',)
    list_display_links = ('id', 'staff', 'status', 'work', 'biko',)
    # list_display = ('staff', 'status', 'work', 'biko',)
    # list_display_links = ('staff', 'status', 'work', 'biko',)

admin.site.register(WhiteBorde, WhiteBordeAdmin)


class StaffAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'name', 'section', 'position',)
    list_display_links = ('id', 'user', 'name', 'section', 'position',)
admin.site.register(Staff, StaffAdmin)


class SectionAdmin(admin.ModelAdmin):
    list_display = ('id', 'section', 'is_edit',)
    list_display_links = ('id', 'section', 'is_edit',)
admin.site.register(Section, SectionAdmin)


class PositionAdmin(admin.ModelAdmin):
    list_display = ('id', 'position', 'is_edit',)
    list_display_links = ('id', 'position', 'is_edit',)
admin.site.register(Position, PositionAdmin)


class PlaceAdmin(admin.ModelAdmin):
    list_display = ('id', 'place',)
    list_display_links = ('id', 'place',)
admin.site.register(Place, PlaceAdmin)
