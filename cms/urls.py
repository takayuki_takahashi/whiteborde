from django.conf.urls import url
from cms import views
from django.contrib.auth import views as auth_views

urlpatterns = [
    # 書籍
    url(r'^book/$', views.book_list, name='book_list'),   # 一覧
    url(r'^book/add/$', views.book_edit, name='book_add'),  # 登録
    url(r'^book/mod/(?P<book_id>\d+)/$', views.book_edit, name='book_mod'),  # 修正
    url(r'^book/del/(?P<book_id>\d+)/$', views.book_del, name='book_del'),   # 削除

    # 感想
    url(r'^impression/(?P<book_id>\d+)/$', views.ImpressionList.as_view(), name='impression_list'),  # 一覧
    url(r'^impression/add/(?P<book_id>\d+)/$', views.impression_edit, name='impression_add'),        # 登録
    url(r'^impression/mod/(?P<book_id>\d+)/(?P<impression_id>\d+)/$', views.impression_edit, name='impression_mod'),  # 修正
    url(r'^impression/del/(?P<book_id>\d+)/(?P<impression_id>\d+)/$', views.impression_del, name='impression_del'),   # 削除

    # ログイン
    url(r'^login/$', auth_views.login, name='login'),


    url(r'^logout-then-login/$',
        auth_views.logout_then_login,
        {
            # 今回はsettings.pyで`LOGIN_URL`を指定したので、ここはコメントアウト
            # 'login_url': reverse_lazy('my:login'),
        },
        name='logout_then_login'
    ),


    # ホワイトボード
    url(r'^whiteborde/$', views.whiteborde_list, name='whiteborde_list'),  # 一覧
    # url(r'^whiteborde/add/$', views.whiteborde_edit, name='whiteborde_add'),  # 登録
    #url(r'^whiteborde/mod/(?P<shain_id>\d+)/$', views.whiteborde_edit, name='whiteborde_mod'),  # 修正

    url(r'^whiteborde/whiteborde_status_save/$', views.whiteborde_status_save, name='whiteborde_status_save'),  # 出退勤の更新
    url(r'^whiteborde/whiteborde_work_save/$', views.whiteborde_work_save, name='whiteborde_work_save'),      # 作業場所の更新
    url(r'^whiteborde/whiteborde_biko_save/$', views.whiteborde_biko_save, name='whiteborde_biko_save'),      # 備考の更新
    # url(r'^whiteborde/whiteborde_status_save/(?P<shain_id>\d+)/(?P<status>\d+)/$', views.whiteborde_status_save, name='whiteborde_status_save'),  # 出勤状況更新

]