import datetime
import requests
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404, redirect

# Create your views here.
from django.views.decorators.http import require_http_methods

from cms.models import Book, Impression, WhiteBorde, Staff, Place
from cms.forms import BookForm, ImpressionForm
from django.views.generic.list import ListView


@login_required
def book_list(request):
    """書籍の一覧"""
    #    return HttpResponse('書籍の一覧')
    books = Book.objects.all().order_by('id')
    return render(request,
                  'cms/book_list.html',  # 使用するテンプレート
                  {'books': books})  # テンプレートに渡すデータ


def book_edit(request, book_id=None):
    """書籍の編集"""
    #     return HttpResponse('書籍の編集')
    if book_id:  # book_id が指定されている (修正時)
        book = get_object_or_404(Book, pk=book_id)
    else:  # book_id が指定されていない (追加時)
        book = Book()

    if request.method == 'POST':
        form = BookForm(request.POST, instance=book)  # POST された request データからフォームを作成
        if form.is_valid():  # フォームのバリデーション
            book = form.save(commit=False)
            book.save()
            return redirect('cms:book_list')
    else:  # GET の時
        form = BookForm(instance=book)  # book インスタンスからフォームを作成

    return render(request, 'cms/book_edit.html', dict(form=form, book_id=book_id))


def book_del(request, book_id):
    """書籍の削除"""
    #     return HttpResponse('書籍の削除')
    book = get_object_or_404(Book, pk=book_id)
    book.delete()
    return redirect('cms:book_list')


class ImpressionList(ListView):
    """感想の一覧"""
    context_object_name='impressions'
    template_name='cms/impression_list.html'
    paginate_by = 2  # １ページは最大2件ずつでページングする

    def get(self, request, *args, **kwargs):
        book = get_object_or_404(Book, pk=kwargs['book_id'])  # 親の書籍を読む
        impressions = book.impressions.all().order_by('id')   # 書籍の子供の、感想を読む
        self.object_list = impressions

        context = self.get_context_data(object_list=self.object_list, book=book)
        return self.render_to_response(context)


def impression_edit(request, book_id, impression_id=None):
    """感想の編集"""
    book = get_object_or_404(Book, pk=book_id)  # 親の書籍を読む
    if impression_id:   # impression_id が指定されている (修正時)
        impression = get_object_or_404(Impression, pk=impression_id)
    else:               # impression_id が指定されていない (追加時)
        impression = Impression()

    if request.method == 'POST':
        form = ImpressionForm(request.POST, instance=impression)  # POST された request データからフォームを作成
        if form.is_valid():    # フォームのバリデーション
            impression = form.save(commit=False)
            impression.book = book  # この感想の、親の書籍をセット
            impression.save()
            return redirect('cms:impression_list', book_id=book_id)
    else:    # GET の時
        form = ImpressionForm(instance=impression)  # impression インスタンスからフォームを作成

    return render(request,
                  'cms/impression_edit.html',
                  dict(form=form, book_id=book_id, impression_id=impression_id))


def impression_del(request, book_id, impression_id):
    """感想の削除"""
    impression = get_object_or_404(Impression, pk=impression_id)
    impression.delete()
    return redirect('cms:impression_list', book_id=book_id)


@login_required
def whiteborde_list(request):
    """WhiteBordeの一覧"""
    whiteborde = WhiteBorde.objects.all().order_by('staff')

    places = Place.objects.all().order_by('id')

    # jsonで取得
    url = 'https://destination-board.herokuapp.com/api/order/?api_key=a9cdeacb-304e-4182-9559-dfb534604397'
    r = requests.get(url)

    member_data = []
    if r.status_code == 200:
        json = r.json()
        for section in json['data']:
            for member in section['member']:
                member_data.append(
                    {'section_order': section['section_order'],
                     'section_name': section['section_name'],
                     'user_id': member['user_id'],
                     'order': member['order']})

    whiteborde_order = []
    for member_datas in member_data:
        for whiteborde_for in whiteborde:
            if str(whiteborde_for.staff.user) == member_datas['user_id']:
                whiteborde_order.append(whiteborde_for)
                break

    try:
        login_user = Staff.objects.get(user=request.user)
    except Staff.DoesNotExist:
        login_user = None

    if login_user.section.is_edit == True or login_user.position.is_edit:
        is_edit = True
    else:
        is_edit = False

    # # 現在日付
    day = datetime.date.today()

    # # YYYYMMDDで表示
    today = day.strftime("%Y%m%d")
    #
    # whiteborde.kairyo = "3"

    return render(request,
                  'cms/whiteborde_list.html',     # 使用するテンプレート
                  {
                      # 'whitebordes': whiteborde,
                      'whitebordes': whiteborde_order,
                      'nowymd': today,
                      'places': places,
                      'login_user': login_user,
                      'is_edit': is_edit
                  })    # テンプレートに渡すデータ

#
# def whiteborde_edit(request, shain_id=None, shain_name=None):
#     """WhiteBordeの編集"""
#     #     return HttpResponse('書籍の編集')
#     if shain_id:  # shain_id が指定されている (修正時)
#         whiteborde = get_object_or_404(WhiteBorde, pk=shain_id)
#     else:  # shain_id が指定されていない (追加時)
#         whiteborde = WhiteBorde()
#
#     if request.method == 'POST':
#         form = WhiteBordeForm(request.POST, instance=whiteborde)  # POST された request データからフォームを作成
#         if form.is_valid():  # フォームのバリデーション
#             whiteborde = form.save(commit=False)
#             whiteborde.save()
#             return redirect('cms:whiteborde_list')
#     else:  # GET の時
#         form = WhiteBordeForm(instance=whiteborde)  # whiteborde インスタンスからフォームを作成
#
#     return render(request, 'cms/whiteborde_edit.html', dict(form=form, shain_id=shain_id))


#def whiteborde_status_save(request, shain_id=None, status=None):

@require_http_methods(['POST'])
def whiteborde_status_save(request):
    # requestからパラメータを受け取る
    # shain_id = request.GET['shain_id']
    # status = request.GET['status']

    if request.method == 'POST':
        shain_id = request.POST['shain_id']
        status = request.POST['status']

        # 更新するwhitebordeテーブルを絞り込む
        whiteborde = get_object_or_404(WhiteBorde, staff__user__username=shain_id)

        # whitebordeテーブルにパラメータを代入する
        whiteborde.status = status
        # whiteborde.updatedate = datetime.datetime.now()

        # whitebordeテーブルを保存する
        whiteborde.save()

    # ホワイトボード一覧画面の再表示
    return redirect('cms:whiteborde_list')


@require_http_methods(['POST'])
def whiteborde_work_save(request):
    # requestからパラメータを受け取る
    # shain_id = request.GET['shain_id']
    # work = request.GET['work']

    if request.method == 'POST':
        shain_id = request.POST['shain_id']
        # work = request.POST['work']

        work = Place.objects.get(place=request.POST['work'])

        # 更新するwhitebordeテーブルを絞り込む
        whiteborde = get_object_or_404(WhiteBorde, staff__user__username=shain_id)

        # whitebordeテーブルにパラメータを代入する
        whiteborde.work = work
        # whiteborde.updatedate = datetime.datetime.now()

        # whitebordeテーブルを保存する
        whiteborde.save()

    # ホワイトボード一覧画面の再表示
    return redirect('cms:whiteborde_list')


@require_http_methods(['POST'])
def whiteborde_biko_save(request):
    # requestからパラメータを受け取る
    # shain_id = request.GET['shain_id']
    # biko = request.GET['biko']

    if request.method == 'POST':
        shain_id = request.POST['shain_id']
        biko = request.POST['biko']


        biko = biko.replace('<br>', '\r\n')

        # 更新するwhitebordeテーブルを絞り込む
        whiteborde = get_object_or_404(WhiteBorde, staff__user__username=shain_id)

        # whitebordeテーブルにパラメータを代入する
        whiteborde.biko = biko
        # whiteborde.updatedate = datetime.datetime.now()

        # whitebordeテーブルを保存する
        whiteborde.save()

    # ホワイトボード一覧画面の再表示
    return redirect('cms:whiteborde_list')
