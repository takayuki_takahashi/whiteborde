from django.forms import ModelForm, DateTimeField
from cms.models import Book, Impression, WhiteBorde


class BookForm(ModelForm):
    """書籍のフォーム"""
    class Meta:
        model = Book
        fields = ('name', 'publisher', 'page', )


class ImpressionForm(ModelForm):
    """感想のフォーム"""
    class Meta:
        model = Impression
        fields = ('comment', )

#
# class WhiteBordeForm(ModelForm):
#     """WhiteBordeのフォーム"""
#
#     updatedate = DateTimeField(input_formats=['%m-%d %H:%M'])
#     #bikoval = 'biko'
#     #biko = bikoval.split("\n").join("<br>");
#
#     class Meta:
#         model = WhiteBorde
#         fields = ('shain_id', 'shain_name', 'status', 'work', 'biko', 'updatedate',)
#         #fields = ('shain_name', 'status', 'biko', )