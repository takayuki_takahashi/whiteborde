
  function sel_status(shainid, loginid, url){
//    var statusval = $("#status" + shainid).val();
//
//    $('#status_save').attr('href', "whiteborde_status_save?shain_id=" + shainid + "&status=" + statusval);
//
//    $("#status_save_button").click();

    if (loginid != shainid) {
        if(!window.confirm('ログインユーザーとは違う社員ですが変更しますか？')){
            window.location.href = ""
            return
        }
    }

    var statusval = $("#status" + shainid).val();
    var token = $("input[name=csrfmiddlewaretoken]").val()

    $.post(url, {
        'csrfmiddlewaretoken': token,
        'shain_id': shainid,
        'status': statusval
    })
    .done(function(){
    })
    .fail(function(){
    })
    .always(function(){
        location.reload();
    });

  }



  function sel_work(shainid, loginid, url){
//    var workval = $("#work" + shainid).val();
//
//    $('#work_save').attr("href", "whiteborde_work_save?shain_id=" + shainid + "&work=" + workval);
//
//    $("#work_save_button").click();
//
    if (loginid != shainid) {
        if(!window.confirm('ログインユーザーとは違う社員ですが変更しますか？')){
            window.location.href = ""
            return
        }
    }

    var workval = $("#work" + shainid).val();
    var token = $("input[name=csrfmiddlewaretoken]").val()

    $.post(url, {
        'csrfmiddlewaretoken': token,
        'shain_id': shainid,
        'work': workval
    })
    .done(function(){
    })
    .fail(function(){
    })
    .always(function(){
        location.reload();
    });
  }



  function btn_clear(shainid, loginid){
      if (loginid != shainid) {
        if(!window.confirm('ログインユーザーとは違う社員ですが変更しますか？')){
            window.location.href = ""
            return
        }
    }

    $("#biko" + shainid).val("");
    biko_edit(shainid);
  }

  function btn_save(shainid, loginid, url){


//```// 備考を更新
//function note_save(login_user, id, url) {
//    // ログインユーザー以外を編集する時はアラートを表示
//    if (login_user != id) {
//        if(!window.confirm('ログインユーザーとは違う社員ですが変更しますか？')){
//            return
//        }
//    }
//
//    var note = $('#note'+id).val();
//    $.post(url, {
//        'csrfmiddlewaretoken': $("input[name=csrfmiddlewaretoken]").val(),
//        'id': id,
//        'note': note
//    }).done(function(){
//    }).fail(function(){
//    }).always(function(){
//        location.reload();
//    });
//}
//```


    // ログインユーザー以外を編集する時はアラートを表示
    if (loginid != shainid) {
        if(!window.confirm('ログインユーザーとは違う社員ですが変更しますか？')){
            window.location.href = ""
            return
        }
    }

    var bikoval = $("#biko" + shainid).val();
    var token = $("input[name=csrfmiddlewaretoken]").val()

    bikoval = bikoval.split("\n").join("<br>");

    $.post(url, {
        'csrfmiddlewaretoken': token,
        'shain_id': shainid,
        'biko': bikoval
    })
    .done(function(){
    })
    .fail(function(){
    })
    .always(function(){
        location.reload();
    });

//    $('#biko_save').attr('href', "whiteborde_biko_save?shain_id=" + shainid + "&biko=" + bikoval);
//
//    $("#biko_save_button").click();
  }

  function biko_edit(shainid){
     var bikoval = $("#biko" + shainid).val();

     var line_count = (bikoval.match(/\n|\r\n/g) || []).length + 1;

     $("#biko" + shainid).attr("rows", line_count);

     // rowsだけだとIEでずれるのでheightも計算
     $('#biko' + shainid).height(20 * line_count);
  }

  var toDoubleDigits = function(num) {
    num += "";
    if (num.length === 1) {
      num = "0" + num;
    }
    return num;
    }

$(function(){
  //なにかしらの処理

  setInterval(clock ,200); //0.5秒毎にclock()を実行

  function clock() {
    $("#view_clock").get(0).innerHTML = getNow();
  }

  function getNow() {
    var now = new Date();
    var year = now.getFullYear();
	var mon = now.getMonth()+1; //１を足すこと
	var day = now.getDate();
	var hour = now.getHours();
	var min = now.getMinutes();
	var sec = now.getSeconds();

	var you = now.getDay(); //曜日(0～6=日～土)

	//曜日の選択肢
	var youbi = new Array("日","月","火","水","木","金","土");

    //出力用
	var s = year + "年" + mon + "月" + day + "日(" + youbi[you] + ") " + hour + "時" + min + "分" + sec + "秒";
	return s;
  }

  try {
//    var i = 0;
//    var shain_id_tr = "";
//    var shain_id = "";
//    while(true){
//      shain_id_tr = myTBL.tBodies[0].children[i].id;
//      shain_id = shain_id_tr.replace("tr", "");
//      biko_edit(shain_id);
//      i = i + 1;

    var i = 2;
    var shain_id_tr = "";
    var shain_id = "";
    while(true){
      shain_id_tr = myTBL.tBodies[0].children[i].id;
      shain_id = shain_id_tr.replace("tr", "");
      biko_edit(shain_id);
      i = i + 3;
    }
  }
  catch(e){

  }
});
